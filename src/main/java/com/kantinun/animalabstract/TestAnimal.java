/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.animalabstract;

/**
 *
 * @author EAK
 */
public class TestAnimal {

    public static void main(String[] args) {
        //Reptile
        Crocodile crocodile = new Crocodile("Croco");
        crocodile.crawl();
        crocodile.eat();
        crocodile.walk();
        crocodile.speak();
        crocodile.sleep();
        
        System.out.println("");
        
        Snake snake = new Snake("Sneak");
        snake.crawl();
        snake.eat();
        snake.walk();
        snake.speak();
        snake.sleep();
        
        System.out.println("");
        
        Animal reptile = snake;
        System.out.println("Reptile is animal ? " + (reptile instanceof Animal));
        System.out.println("Reptile is land animal ? " + (reptile instanceof LandAnimal));
        System.out.println("Reptile is aquatic animal ? " + (reptile instanceof AquaticAnimal));
        System.out.println("Reptile is reptile ? " + (reptile instanceof Reptile));
        System.out.println("Reptile is poultry ? " + (reptile instanceof Poultry));

        System.out.println("");
        //Land Animal
        Human human = new Human("Dio");
        human.run();
        human.eat();
        human.walk();
        human.speak();
        human.sleep();
        
        System.out.println("");
        
        Dog dog = new Dog("Doge");
        dog.run();
        dog.eat();
        dog.walk();
        dog.speak();
        dog.sleep();
        
        System.out.println("");
        
        Cat cat = new Cat("Mimi");
        cat.run();
        cat.eat();
        cat.walk();
        cat.speak();
        cat.sleep();
        
        Animal land = cat;
        
        System.out.println("");
        
        System.out.println("LandAnimal is animal ? " + (land instanceof Animal));
        System.out.println("LandAnimal is land animal ? " + (land instanceof LandAnimal));
        System.out.println("LandAnimal is aquatic animal ? " + (land instanceof AquaticAnimal));
        System.out.println("LandAnimal is reptile ? " + (land instanceof Reptile));
        System.out.println("LandAnimal is poultry ? " + (land instanceof Poultry));
        
        System.out.println("");
        //Aquatic Animal
        Fish fish = new Fish("Aqua");
        fish.swim();
        fish.eat();
        fish.walk();
        fish.speak();
        fish.sleep();
        
        System.out.println("");
        
        Crab crab = new Crab("Holy");
        crab.swim();
        crab.eat();
        crab.walk();
        crab.speak();
        crab.sleep();
        
        System.out.println("");
        
        Animal aquatic = crab;
        System.out.println("AquaticAnimal is animal ? " + (aquatic instanceof Animal));
        System.out.println("AquaticAnimal is land animal ? " + (aquatic instanceof LandAnimal));
        System.out.println("AquaticAnimal is aquatic animal ? " + (aquatic instanceof AquaticAnimal));
        System.out.println("AquaticAnimal is reptile ? " + (aquatic instanceof Reptile));
        System.out.println("AquaticAnimal is poultry ? " + (aquatic instanceof Poultry));
        
        System.out.println("");
        //Poultry
        Bat bat = new Bat("Boi");
        bat.fly();
        bat.eat();
        bat.walk();
        bat.speak();
        bat.sleep();
        
        System.out.println("");
        
        Bird bird = new Bird("owo");
        bird.fly();
        bird.eat();
        bird.walk();
        bird.speak();
        bird.sleep();
        
        System.out.println("");
        
        Animal poultry = bird;
        System.out.println("Poultry is animal ? " + (poultry instanceof Animal));
        System.out.println("Poultry is land animal ? " + (poultry instanceof LandAnimal));
        System.out.println("Poultry is aquatic animal ? " + (poultry instanceof AquaticAnimal));
        System.out.println("Poultry is reptile ? " + (poultry instanceof Reptile));
        System.out.println("Poultry is poultry ? " + (poultry instanceof Poultry));
    }
}
